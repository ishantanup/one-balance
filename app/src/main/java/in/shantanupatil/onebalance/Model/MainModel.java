package in.shantanupatil.onebalance.Model;

public class MainModel {
    String title;
    String code;

    public MainModel(String title, String code) {
        this.title = title;
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public String getCode() {
        return code;
    }
}
