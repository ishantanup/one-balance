package in.shantanupatil.onebalance;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Launcher extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        TextView app_name_launch_l = (TextView) findViewById(R.id.app_name_launch_l);
        app_name_launch_l.setTypeface(Typeface.createFromAsset(getAssets(), "circularstd.ttf"));
        app_name_launch_l.setTextColor(Color.BLACK);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Launcher.this, MainActivity.class);
                startActivity(intent);
            }
        }, 5000);
    }
}
