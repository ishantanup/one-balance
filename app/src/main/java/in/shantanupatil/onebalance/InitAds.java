package in.shantanupatil.onebalance;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;

public class InitAds extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize AdMob app
        MobileAds.initialize(this, getString(R.string.admob_app_id));
    }
}
