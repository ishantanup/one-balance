package in.shantanupatil.onebalance.DataSource;

public class Airtel {
    String[] title = {
        "Main Menu",
        "Main Balance",
        "My Own Number",
        "Data Balance",
        "Offers",
        "Current Plan",
        "Best Internet Offer",
        "Activate 3G",
        "Data Charged Details",
        "Roaming Packs",
        "Packs (Voice,Data,SMS, Daily)",
        "Start Any Service",
        "Stop Any Service",
        "Hello Tunes, Facebook Live, Aarti, Horoscope",
        "VAS Charged Details",
        "Transfer Balance",
        "Voice Calls Details, SMS/MMS Deduction, Last Recharge History",
        "Customer Care",
        "Complaint Helpline"
    };

    String[] code = {
        "*121#",
        "*121*2#",
        "*121*9#",
        "*121*2#",
        "*121*1#",
        "*121*13#",
        "*121*11#",
        "3G To 121",
        "*121*7#",
        "*121*14#",
        "*121*12#",
        "START To 121",
        "STOP To 121",
        "*121*4#",
        "*121*7#",
        "*141#",
        "*121*7#",
        "121",
        "198"
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}
