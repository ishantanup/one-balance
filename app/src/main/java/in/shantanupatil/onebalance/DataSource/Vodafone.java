package in.shantanupatil.onebalance.DataSource;

public class Vodafone {
    String[] title = {
         "Main Balance",
         "2G and 3G Data Balance",
         "Bonus/Promotional, Night Minutes,SMS,Local, STD Call Rate",
         "My Own Number",
         "Activate 3G",
         "GPRS Data Balance",
         "Best Internet Offer",
         "2G/3G Recharge Dial",
         "3G Pack - 20GB",
         "Vodafone Offer",
         "Best Offers",
         "Last 3 Activities",
         "Last 3 charges",
         "Tariff Plan Details",
         "My Plan Rates",
         "Active Plan Validity",
         "Customer  Care",
         "Compalint Helpline",
    };

    String[] code = {
        "*141#",
        "*111*2*2#",
        "*111*4*2#",
        "*111*2#",
        "3G To 144",
        "*111*2*2#",
        "*111*1*5#",
        "*121#",
        "*444*8#",
        "*111*1*1#",
        "*121#",
        "*111*2*3#",
        "*111*3*2#",
        "*111*4#",
        "*111*4*1#",
        "*111*4*3#",
        "111",
        "198"
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}
