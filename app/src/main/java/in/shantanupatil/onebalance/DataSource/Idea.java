package in .shantanupatil.onebalance.DataSource;

public class Idea {

    public Idea() {
    }

    String[] title = {
        "Main Menu",
        "Main Balance",
        "My Number",
        "Data Balance",
        "SMS Balance",
        "Activate 3G",
        "DND",
        "2G Data Pack",
        "4G Activate",
        "Rs 5 Talktime Credit",
        "Rs 10 Talktime Credit",
        "Rs 20 Talktime Credit",
        "Best Offer Check",
        "Customer Care",
        "Complaint Helpline",
        "Last 5 Call Details",
        "Last Recharge Details",
        "Last 5 SMS Details",
        "Last 5 Data Session Details",
        "Activate Caller Tune"
    };

    String[] code = {
       "*121*4#",
       "*121#",
       "*1#",
       "*125#",
       "*161*1#",
       "ACT 3G To 12345",
       "1909",
       "*369#",
       "1925",
       "*150*05#",
       "*150*10#",
       "*150*20#",
       "*121#",
       "12345",
       "198",
       "*121*4*1*1#",
       "*121*4*1*8#",
       "*121*4*1*2#",
       "*121*4*1*3#",
       "56789"
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}