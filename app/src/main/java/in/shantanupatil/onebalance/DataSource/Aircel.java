package in.shantanupatil.onebalance.DataSource;

public class Aircel {
    String[] title = {
            "Main Balance ",
            "My Number",
            "Special Offers",
            "Take Advance TalkTime",
            "Activate 3G/4G",
            "Customer Care",
            "Latest Offers",
            "Check Data Balance",
            "Activate 3G",
            "DeActivate 3G"
    };

    String[] code = {
            "*121#",
            "*133#",
            "*121*1#",
            "*414#",
            "3G To 121",
            "121",
            "1213",
            "*133#",
            "START 3G To 121",
            "STOP 3G To 121"
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}
