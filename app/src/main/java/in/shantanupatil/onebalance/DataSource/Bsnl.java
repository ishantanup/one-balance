package in.shantanupatil.onebalance.DataSource;

public class Bsnl {
    String[] title = {
        "Main Balance",
        "Plan Change",
        "Balance And Validity",
        "Customer Care",
        "Take Advance Talktime",
        "My Own Number",
        "Current Call Rate",
        "Last Call",
        "Tariff Plan Check",
        "Caller Tune"
    };

    String[] code = {
        "*124#",
        "*124#",
        "*123#",
        "1503",
        "*518#",
        "*1#",
        "*123*3#",
        "*124*3#",
        "*124*4#",
        "*567#",
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}
