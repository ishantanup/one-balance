package in.shantanupatil.onebalance.DataSource;

public class Jio {

    String[] title = {
        "Know balance",
        "Call To",
        "Jio Number",
        "Caller Tune Activation Code",
        "Deactivate Jio Caller Tune",
        "Check Call Rate"
    };

    String[] code = {
        "*333#",
        "1299",
        "*1#",
        "Caller Tune Activation Code",
        "*333*3*1*2#",
        "sms TARIFF to 191"
    };

    public String getTitle(int i) {
        return title[i];
    }

    public String getCode(int i) {
        return code[i];
    }

    public int getSize() {
        return code.length;
    }
}
