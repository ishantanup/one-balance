package in.shantanupatil.onebalance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import in.shantanupatil.onebalance.DataSource.Aircel;
import in.shantanupatil.onebalance.DataSource.Airtel;
import in.shantanupatil.onebalance.DataSource.Bsnl;
import in.shantanupatil.onebalance.DataSource.Idea;
import in.shantanupatil.onebalance.DataSource.Jio;
import in.shantanupatil.onebalance.DataSource.Vodafone;
import in.shantanupatil.onebalance.Model.MainModel;
import in.shantanupatil.onebalance.R;
import in.shantanupatil.onebalance.SimAdapter;

public class SimDetails extends AppCompatActivity implements SimAdapter.IPassData{

    private RecyclerView simRecycler;
    private ArrayList<MainModel> mainModels;
    private SimAdapter adapter;

    private SimAdapter.Controller controller;

    private AdView adView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim_details);
        Log.d("AdsS", "onCreate: ");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_sim);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //setting the InterstitialAd
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
        AdRequest inAdRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(inAdRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });

        //setting the banner ad
        AdRequest bannerAd = new AdRequest.Builder().build();
        adView = (AdView) findViewById(R.id.ad_banner);
        adView.loadAd(bannerAd);

        initViews();

        controller = new SimAdapter.Controller(this);

        Intent intent = getIntent();
        String sim = intent.getStringExtra("sim");
        toolbar.setTitle(sim.toString());

        //create Array for main models
        mainModels = new ArrayList<>();

        if (sim.equals("idea")) {
            Idea idea = new Idea();
            for (int i = 0; i < idea.getSize(); i++) {
                MainModel mainModel = new MainModel(idea.getTitle(i), idea.getCode(i));
                mainModels.add(mainModel);
            }
        } else if (sim.equals("bsnl")){
            Bsnl bsnl = new Bsnl();
            for (int i = 0; i < bsnl.getSize(); i++) {
                MainModel mainModel = new MainModel(bsnl.getTitle(i), bsnl.getCode(i));
                mainModels.add(mainModel);
            }
        } else if (sim.equals("aircel")){
            Aircel aircel = new Aircel();
            for (int i = 0; i < aircel.getSize(); i++) {
                MainModel mainModel = new MainModel(aircel.getTitle(i), aircel.getCode(i));
                mainModels.add(mainModel);
            }
        } else if (sim.equals("jio")){
            Jio jio = new Jio();
            for (int i = 0; i < jio.getSize(); i++) {
                MainModel mainModel = new MainModel(jio.getTitle(i), jio.getCode(i));
                mainModels.add(mainModel);
            }
        } else if (sim.equals("airtel")){
            Airtel airtel = new Airtel();
            for (int i = 0; i < airtel.getSize(); i++) {
                MainModel mainModel = new MainModel(airtel.getTitle(i), airtel.getCode(i));
                mainModels.add(mainModel);
            }
        } else if (sim.equals("vodafone")){
            Vodafone vodafone = new Vodafone();
            for (int i = 0; i < vodafone.getSize(); i++) {
                MainModel mainModel = new MainModel(vodafone.getTitle(i), vodafone.getCode(i));
                mainModels.add(mainModel);
            }
        }

        adapter = new SimAdapter(mainModels, controller);
        simRecycler.setAdapter(adapter);

    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        showInterstitial();
        if (adView != null) {
            adView.resume();
        }
        super.onResume();
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    private void initViews() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");

        //recyclerView setup
        simRecycler = (RecyclerView) findViewById(R.id.sim_recycler);
        simRecycler.setHasFixedSize(true);
        simRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    @SuppressLint("MissingPermission")
    @Override
    public void passData(String title, String code) {
        if(code.startsWith("*")) {
            String ussdCode = code.replace("#", "") + "%23";
            startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + ussdCode)));
        } else if (Character.isDigit(code.charAt(0))) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + code));
            startActivity(callIntent);
        }
    }
}
