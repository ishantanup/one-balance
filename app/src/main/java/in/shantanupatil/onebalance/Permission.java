package in.shantanupatil.onebalance;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Permission extends AppCompatActivity {

    Button grant_permission;
    LinearLayout linearLayout;
    TextView app_name_launch;

    private static int REQUEST_EXTERNAL_PERMISSION = 1;
    String[] perms = {"android.permission.CALL_PHONE", "android.permission.READ_PHONE_STATE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        grant_permission = (Button) findViewById(R.id.grant_permission);
        app_name_launch = (TextView) findViewById(R.id.app_name_launch);
        app_name_launch.setTypeface(Typeface.createFromAsset(getAssets(), "circularstd.ttf"));
        app_name_launch.setTextColor(Color.BLACK);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        grant_permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(perms, REQUEST_EXTERNAL_PERMISSION);
                } else {
                    Intent intent = new Intent(Permission.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Permission.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(linearLayout, "Please Grant Permission", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        }
    }
}
