package in.shantanupatil.onebalance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.shantanupatil.onebalance.Model.MainModel;

public class SimAdapter extends RecyclerView.Adapter<SimAdapter.ViewHolder>{

    ArrayList<MainModel> mainModels;
    Controller controller;
    private final int LIST = 1;
    private final int HEAD = 0;

    public SimAdapter(ArrayList<MainModel> mainModels, Controller controller) {
        this.mainModels = mainModels;
        this.controller = controller;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view;
        ViewHolder viewHolder;

        if (viewType == LIST) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.offer_item, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;

        } else if (viewType == HEAD) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_first, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MainModel mainModel = mainModels.get(position);
        if (holder.view_type == LIST) {
            holder.offerTitle.setText(mainModel.getTitle());
            holder.offerCode.setText(mainModel.getCode());
        } else if (holder.view_type == HEAD) {
            holder.offerTitle_HEAD.setText(mainModel.getTitle());
            holder.offerCode_HEAD.setText(mainModel.getCode());
        }
    }

    @Override
    public int getItemCount() {
        return mainModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEAD;
        return LIST;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView offerTitle_HEAD;
        TextView offerCode_HEAD;
        LinearLayout linearLayout_HEAD;

        TextView offerTitle;
        TextView offerCode;
        LinearLayout linearLayout;

        int view_type;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            if (viewType == LIST) {
                offerTitle = (TextView) itemView.findViewById(R.id.offer_title);
                offerCode = (TextView) itemView.findViewById(R.id.offer_code);
                linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout_list);
                linearLayout.setOnClickListener(this);
                view_type = 1;
            } else if (viewType == HEAD) {
                offerTitle_HEAD = (TextView) itemView.findViewById(R.id.offerTitle_HEAD);
                offerCode_HEAD = (TextView) itemView.findViewById(R.id.offerCode_HEAD);
                linearLayout_HEAD = (LinearLayout) itemView.findViewById(R.id.linearLayout_HEAD);
                linearLayout_HEAD.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            MainModel mainModel = mainModels.get(getAdapterPosition());
            controller.OnItemClcik(mainModel.getTitle(), mainModel.getCode());
        }
    }

    public interface IPassData {
        void passData(String title, String code);
    }

    public static class Controller {
        IPassData iPassData;

        public Controller(IPassData iPassData) {
            this.iPassData = iPassData;
        }

        public void OnItemClcik(String title, String code) {
            iPassData.passData(title, code);
        }
    }
}
