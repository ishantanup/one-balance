package in.shantanupatil.onebalance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity{

    CollapsingToolbarLayout collapsingToolbarLayout;
    CardView userCard;
    TextView sim;

    private AdView adView;

    final String TAG = "DubuggingS";
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (hasPermission()) {
            adView = (AdView) findViewById(R.id.banner_home);
            //setting the banner ad
            AdRequest bannerAd = new AdRequest.Builder().build();
            adView.loadAd(bannerAd);

            Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");

            //Initialize views
            initViews(typeface);

            ButterKnife.bind(this);

            //get the sim from user
            getSimState();

            userCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String[] sims = sim.getText().toString().split(" ");
                    openActivity(sims[0]);
                }
            });
        } else {
            Intent intent = new Intent(getApplicationContext(), Permission.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean hasPermission() {
        int call_phone = this.checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE);
        int phone_state = this.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE);
        return call_phone == PackageManager.PERMISSION_GRANTED && phone_state == PackageManager.PERMISSION_GRANTED;
    }


    //Get the mode of screen landscape//portrait
    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }
    //initialize views

    private void initViews(Typeface typeface) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);
        sim = (TextView) findViewById(R.id.sim);
        userCard = (CardView) findViewById(R.id.user_card);
        userCard.setVisibility(View.GONE);
        if (getRotation(getApplicationContext()).equals("landscape") || getRotation(getApplicationContext()).equals("reverse landscape")) {
            collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_home);
            collapsingToolbarLayout.setTitle("One Balance");
            collapsingToolbarLayout.setExpandedTitleTypeface(typeface);
            collapsingToolbarLayout.setExpandedTitleTextColor(ColorStateList.valueOf(Color.BLACK));
            sim.setTypeface(typeface);
            getSupportActionBar().setTitle("");
        }
        if (getRotation(getApplicationContext()).equals("portrait") || getRotation(getApplicationContext()).equals("reverse portrait")) {
            TextView app_name = (TextView) findViewById(R.id.app_name);
            app_name.setTypeface(typeface);
            getSupportActionBar().setTitle("");
        }
    }
    //Add onclick listeners to button

    @OnClick({R.id.airtel, R.id.aircel, R.id.bsnl, R.id.idea, R.id.jio, R.id.vodafone, R.id.refer, R.id.rate})
    public void callbackClicked(View v) {
        switch (v.getId()) {
            case R.id.airtel:
                openActivity("Airtel");
                break;
            case R.id.aircel:
                openActivity("Aircel");
                break;
            case R.id.bsnl:
                openActivity("Bsnl");
                break;
            case R.id.idea:
                openActivity("Idea");
                break;
            case R.id.jio:
                openActivity("Jio");
                break;
            case R.id.vodafone:
                openActivity("Vodafone");
                break;
            case R.id.refer:
                final String packageName = getPackageName(); // getPackageName() from Context or Activity object
                String app = "https://play.google.com/store/apps/details?id=" + packageName;

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                String message = "Download One Balance and stay tuned with codes to check balance, last 5 calls etc.,\n\nLink: https://play.google.com/store/apps/details?id=" + getPackageName();
                share.putExtra(Intent.EXTRA_TEXT, "" + message);
                startActivity(Intent.createChooser(share, "Share Via"));
                break;
            case R.id.rate:
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;

        }
    }
    //Menus

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            inflater.inflate(R.menu.upper_menu_newapi, menu);
            return true;
        } else {
            inflater.inflate(R.menu.upper_menu, menu);
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Toast.makeText(this, "About Selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.change_sim:
                selectSimCard();
                return true;
            case R.id.change_sim_newapi:
                List<SubscriptionInfo> subscriptionInfoList = null;
                if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    SubscriptionManager subscriptionManager = (SubscriptionManager) getApplicationContext().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                    subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                    if (subscriptionInfoList != null && subscriptionInfoList.size() > 0) {
                        if (subscriptionInfoList.size() == 1) {
                            Toast.makeText(this, "Only One Sim Detected", Toast.LENGTH_SHORT).show();
                        } else {
                            getNewSim(subscriptionInfoList);
                        }
                    } else {
                        selectSimCard();
                    }
                }
                return true;
        }
        return false;
    }


    private void selectSimCard() {
        String[] items = {
                "Idea",
                "Aircel",
                "Airtel",
                "JIO",
                "BSNL",
                "Vodafone"
        };
        new LovelyChoiceDialog(this)
                .setTopColorRes(R.color.cardview_shadow_end_color)
                .setTitle("Select Your Sim")
                .setItemsMultiChoice(items, new LovelyChoiceDialog.OnItemsSelectedListener<String>() {
                    @Override
                    public void onItemsSelected(List<Integer> positions, List<String> items) {
                        if (items.size() != 0) {
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("SIM", items.get(0));
                            editor.commit();

                            setVisibleSim(preferences.getString("SIM", ""));
                            Toast.makeText(MainActivity.this, "Sim changed to: " + preferences.getString("SIM", ""), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Please Select Something", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setConfirmButtonText("Okay")
                .show();
    }
    public void getNewSim(List<SubscriptionInfo> subscriptionInfoList) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            String[] items = new String[2];
            if (subscriptionInfoList.size() == 1) {
                setVisibleSim(subscriptionInfoList.get(0).getCarrierName().toString());
            } else {
                items = new String[]{
                        subscriptionInfoList.get(0).getCarrierName().toString(),
                        subscriptionInfoList.get(1).getCarrierName().toString()
                };

                new LovelyChoiceDialog(this)
                        .setTopColorRes(R.color.cardview_shadow_end_color)
                        .setTitle("Select Your Sim")
                        .setItemsMultiChoice(items, new LovelyChoiceDialog.OnItemsSelectedListener<String>() {
                            @Override
                            public void onItemsSelected(List<Integer> positions, List<String> items) {
                                if (items.size() != 0) {
                                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("NEWSIM", items.get(0));
                                    editor.commit();
                                    Toast.makeText(MainActivity.this, "Sim changed to: " + preferences.getString("NEWSIM", ""), Toast.LENGTH_SHORT).show();
                                    setVisibleSim(preferences.getString("NEWSIM", ""));
                                } else {
                                    Toast.makeText(MainActivity.this, "Please Select Something", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setConfirmButtonText("Okay")
                        .show();

            }
        }
    }

    private void openActivity(String sim) {
        Intent intent = new Intent(getApplicationContext(), SimDetails.class);
        intent.putExtra("sim", sim.toLowerCase());
        startActivity(intent);
    }


    public void getSimState() {
        List<SubscriptionInfo> subscriptionInfoList = null;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = (SubscriptionManager) getApplicationContext().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
            if (subscriptionInfoList != null && subscriptionInfoList.size() > 0) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String carrierName = preferences.getString("NEWSIM", "");
                if (carrierName.equals(""))
                    getNewSim(subscriptionInfoList);
                else
                    setVisibleSim(carrierName);
            } else {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String SIM = preferences.getString("SIM", "");
                if (SIM.equals(""))
                    selectSimCard();
                else
                    setVisibleSim(SIM);
            }
        } else {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String SIM =  preferences.getString("SIM", "");
            if (SIM.equals("")) {
                selectSimCard();
            } else {
                setVisibleSim(SIM);
            }
        }

    }

    public void setVisibleSim(String carrierName) {
        carrierName = carrierName.toLowerCase();
        Log.d("HelloDebugging", "setVisibleSim: " + carrierName);
        if (carrierName.startsWith("airtel")) {
            ((Button) findViewById(R.id.airtel)).setVisibility(View.GONE);
        } else if (carrierName.startsWith("aircel")) {
            ((Button) findViewById(R.id.aircel)).setVisibility(View.GONE);
        } else if (carrierName.startsWith("bsnl")) {
            ((Button) findViewById(R.id.bsnl)).setVisibility(View.GONE);
        } else if (carrierName.startsWith("idea")) {
            ((Button) findViewById(R.id.idea)).setVisibility(View.GONE);
        } else if (carrierName.startsWith("jio")) {
            ((Button) findViewById(R.id.jio)).setVisibility(View.GONE);
        } else if (carrierName.startsWith("vodafone")) {
            ((Button) findViewById(R.id.vodafone)).setVisibility(View.GONE);
        }
        userCard.setVisibility(View.VISIBLE);
        sim.setText(carrierName.substring(0,1).toUpperCase() + carrierName.substring(1, carrierName.length()));
    }
}
